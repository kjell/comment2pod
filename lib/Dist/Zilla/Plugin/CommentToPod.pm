package Dist::Zilla::Plugin::CommentToPod;

# ABSTRACT: disstzilla plugin for App::CommentToPod - turns comments to pod
use Moose;
with(
	'Dist::Zilla::Role::FileMunger',
	'Dist::Zilla::Role::FileFinderUser' => {
		default_finders => [':InstallModules'],
	},
);

sub munge_files {
	my $self = shift;
	$self->munge_file($_) for @{ $self->found_files };
}

sub munge_file {
	my ($self, $file) = @_;

	$self->log_debug([ 'pod\'ing %s', $file->name ]);

	require App::CommentToPod;
	my $pm = App::CommentToPod->new;
	$pm->addPod($file->content);
	$file->content($pm->podfile);

	return;
}

1;
